package tcc.ufc.br.maoamiga.model

/**
* Created by Arthur Sousa on 08/04/18.
*/

class Announcement(val id: String, val day: String, val month: String, val title: String, val description: String)