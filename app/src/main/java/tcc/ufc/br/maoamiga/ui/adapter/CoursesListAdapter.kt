package tcc.ufc.br.maoamiga.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.course_item.view.*
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Course
import android.view.inputmethod.InputMethodManager

/**
* Created by Arthur Sousa on 15/04/18.
*/

class CoursesListAdapter(private val courses: MutableList<Course>,
                         private val context: Context) : Adapter<CoursesListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.course_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val course = courses[position]

        holder?.let {
            it.code.text = course.code
            it.name.text = course.name

            it.courseEquivalents.text = course.equivalents
            it.courseRequirements.text = course.requirements
            it.coursePrivateSpots.text = course.private_spots

            val courseDetails = it.courseDetails

            it.detailsButton.setOnClickListener {
                it.visibility = View.GONE
                courseDetails.visibility = View.VISIBLE

                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(it.windowToken, 0)
            }
        }

    }

    override fun getItemCount(): Int {
        return courses.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val code: TextView = itemView.course_item_code
        val name: TextView = itemView.course_item_name

        val detailsButton: Button = itemView.action_course_details_button
        val courseDetails: LinearLayout = itemView.course_details

        val courseEquivalents: TextView = itemView.course_item_equivalents
        val courseRequirements: TextView = itemView.course_item_requirements
        val coursePrivateSpots: TextView = itemView.course_item_private_spots
    }

}
