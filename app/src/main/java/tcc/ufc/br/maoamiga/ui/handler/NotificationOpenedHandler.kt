package tcc.ufc.br.maoamiga.ui.handler

import android.app.Application
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal
import tcc.ufc.br.maoamiga.view.BottomNavigationActivity
import android.content.Intent

/**
 * Created by Arthur Sousa on 02/06/18.
 */

class NotificationOpenedHandler(private val application: Application) : OneSignal.NotificationOpenedHandler {

    override fun notificationOpened(result: OSNotificationOpenResult) {
        val intent = Intent(application, BottomNavigationActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK)
        application.startActivity(intent)
    }

}