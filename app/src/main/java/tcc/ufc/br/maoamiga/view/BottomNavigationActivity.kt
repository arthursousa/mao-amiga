package tcc.ufc.br.maoamiga.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import tcc.ufc.br.maoamiga.R
import android.view.LayoutInflater
import android.view.View
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import kotlinx.android.synthetic.main.notification_badge.view.*
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseReference
import com.onesignal.OneSignal
import tcc.ufc.br.maoamiga.ui.handler.*

/**
* Created by Arthur Sousa on 05/04/18.
*/

class BottomNavigationActivity : AppCompatActivity() {

    private var content: FrameLayout? = null

    private var mDatabase: DatabaseReference? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {

                val fragment = FragmentHome.newInstance()
                addFragment(fragment)

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_requests -> {
                val fragment = SubmitRequestFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_inquiries -> {
                val fragment = InquiriesFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                val fragment = NotificationsFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_student -> {
                val fragment = StudentFragment()
                addFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    /**
     * add/replace fragment in container FrameLayout
     */
    @SuppressLint("PrivateResource")
    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.design_bottom_sheet_slide_in, R.anim.design_bottom_sheet_slide_out)
                .replace(R.id.content, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        content = findViewById(R.id.content)
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        mDatabase = FirebaseDatabase.getInstance().reference

        val fragment = FragmentHome.newInstance()
        addFragment(fragment)

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(NotificationOpenedHandler(application))
                .init()
    }

    fun refreshBadge() {
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)
        val bottomNavigationMenuView = navigation.getChildAt(0) as BottomNavigationMenuView

        val newBadge = LayoutInflater.from(this)
                .inflate(R.layout.notification_badge, bottomNavigationMenuView, false)
        newBadge.tag = "badge"

        // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
        mDatabase?.child("students")?.child("one")?.child("notifications")?.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var count = 0

                for (snapshot in dataSnapshot.children) {
                    if (!(snapshot.child("read").value as Boolean)) {
                        count++
                    }
                }

                if (count == 0) {
                    return
                }

                val notificationMenuItem = bottomNavigationMenuView.getChildAt(3)
                val itemView = notificationMenuItem as BottomNavigationItemView

                val currentBadge = itemView.findViewWithTag<View>("badge")

                if (currentBadge == null) {
                    newBadge.badge_text_view.text = count.toString()
                    itemView.addView(newBadge)
                    return
                }

                currentBadge.badge_text_view.text = count.toString()
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    fun removeBadge() {
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)
        val bottomNavigationMenuView = navigation.getChildAt(0) as BottomNavigationMenuView

        val notificationMenuItem = bottomNavigationMenuView.getChildAt(3)
        val itemView = notificationMenuItem as BottomNavigationItemView

        if (itemView.findViewWithTag<View>("badge") == null) {
            return
        }

        itemView.removeViewAt(itemView.childCount - 1)
    }

}
