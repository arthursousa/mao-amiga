package tcc.ufc.br.maoamiga

import com.google.firebase.database.FirebaseDatabase

/**
 * Created by Arthur Sousa on 02/06/18.
 */

class MaoAmigaApp : android.app.Application() {

    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
    }

}

