package tcc.ufc.br.maoamiga.model

/**
* Created by Arthur Sousa on 08/04/18.
*/

class RequestType(val name: String, val course_fields: Boolean = false)