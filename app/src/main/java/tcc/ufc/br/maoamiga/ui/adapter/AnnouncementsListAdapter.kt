package tcc.ufc.br.maoamiga.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.announcement_item.view.*
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Announcement

/**
* Created by Arthur Sousa on 15/04/18.
*/

class AnnouncementsListAdapter(private val announcements: MutableList<Announcement>,
                          private val context: Context) : Adapter<AnnouncementsListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.announcement_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val announcement = announcements[position]

        holder?.let {
            it.day.text = announcement.day
            it.month.text = announcement.month
            it.title.text = announcement.title
            it.description.text = announcement.description
        }
    }

    override fun getItemCount(): Int {
        return announcements.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val day: TextView = itemView.announcement_item_day
        val month: TextView = itemView.announcement_item_month
        val title: TextView = itemView.announcement_item_title
        val description: TextView = itemView.announcement_item_description
    }
}
