package tcc.ufc.br.maoamiga.model

/**
* Created by Arthur Sousa on 08/04/18.
*/

class Course(val code: String, val name: String, val equivalents: String, val requirements: String, val private_spots: String)