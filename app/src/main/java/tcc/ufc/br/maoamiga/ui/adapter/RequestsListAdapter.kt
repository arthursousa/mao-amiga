package tcc.ufc.br.maoamiga.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Button
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.request_item.view.*
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Request
import tcc.ufc.br.maoamiga.model.Status

/**
* Created by Arthur Sousa on 08/04/18.
*/

class RequestsListAdapter(private val requests: MutableList<Request>,
                          private val context: Context) : Adapter<RequestsListAdapter.ViewHolder>() {

    private var mDatabase: DatabaseReference? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        mDatabase = FirebaseDatabase.getInstance().reference

        val view = LayoutInflater.from(context).inflate(R.layout.request_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val request = requests[position]
        holder?.let {
            it.title.text = request.title

            val status = when (request.status) {
                "pending" -> "Status: Pendente"
                "ready" -> "Status: Concluído"
                else -> {
                    "Status: Cancelado"
                }
            }

            it.status.text = status

            if (!request.course_code.isNullOrBlank() && !request.course_name.isNullOrBlank()) {
                val course = request.course_code + " - " + request.course_name
                it.course.text = course
            }

            if (request.status == Status.PENDING.value) {
                it.cancelButton.visibility = View.VISIBLE
            }

            it.cancelButton.setOnClickListener {
                val cancelledRequest = requests[position]

                cancelledRequest.status = Status.CANCELLED.value
                notifyDataSetChanged()

                it.visibility = View.INVISIBLE

                // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
                mDatabase?.child("students")?.child("one")?.child("requests")?.child(cancelledRequest.id)?.setValue(cancelledRequest)
            }
        }
    }

    override fun getItemCount(): Int {
        return requests.size
    }

    // To Do: Allow user to remove it on his phone but keep it in the database
    //    private fun removeItemAt(position: Int) {
    //        requests.removeAt(position)
    //        notifyItemRemoved(position)
    //        notifyItemRangeChanged(position, itemCount)
    //
    //        if (itemCount == 0) {
    //            emptyView.visibility = View.VISIBLE
    //        }
    //    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.request_item_title
        val status: TextView = itemView.request_item_status
        val course: TextView = itemView.request_item_course

        val cancelButton: Button = itemView.action_cancel_button
    }
}