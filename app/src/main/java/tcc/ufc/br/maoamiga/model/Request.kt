package tcc.ufc.br.maoamiga.model

/**
* Created by Arthur Sousa on 08/04/18.
*/

enum class Status(val value : String) {
    CANCELLED("cancelled"), PENDING("pending"), READY("ready")
}

class Request(var id: String, val title: String, var status: String, var note: String? = null, var course_code: String? = null,
              var course_name: String? = null)