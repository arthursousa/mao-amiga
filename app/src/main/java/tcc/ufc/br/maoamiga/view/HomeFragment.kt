package tcc.ufc.br.maoamiga.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Request
import tcc.ufc.br.maoamiga.model.Announcement
import tcc.ufc.br.maoamiga.ui.adapter.RequestsListAdapter
import tcc.ufc.br.maoamiga.ui.adapter.AnnouncementsListAdapter
import android.support.v7.widget.LinearLayoutManager
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_home.*

/**
* Created by Arthur Sousa on 05/04/18.
*/

class FragmentHome : Fragment() {
    private var mDatabase: DatabaseReference? = null

    companion object {
        fun newInstance(): FragmentHome {
            val fragmentHome = FragmentHome()
            val args = Bundle()
            fragmentHome.arguments = args
            return fragmentHome
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDatabase = FirebaseDatabase.getInstance().reference

        (activity as BottomNavigationActivity).refreshBadge()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val announcementsRecyclerView = announcements_list_recyclerview
        announcementsRecyclerView.adapter = AnnouncementsListAdapter(mutableListOf(), activity)

        val announcementsLayoutManager = LinearLayoutManager(activity)
        announcementsRecyclerView.layoutManager = announcementsLayoutManager

        mDatabase?.child("announcements")?.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val announcements = mutableListOf<Announcement>()

                for (snapshot in dataSnapshot.children) {
                    val id = snapshot.key

                    val day = snapshot.child("day").value as String
                    val month = snapshot.child("month").value as String
                    val title = snapshot.child("title").value as String
                    val description = snapshot.child("description").value as String

                    announcements.add(Announcement(id.toString(), day, month, title, description))
                }

                announcementsRecyclerView.adapter = AnnouncementsListAdapter(announcements, activity)

                if (announcements_empty_view != null && announcementsRecyclerView.adapter.itemCount > 0) {
                    announcements_empty_view.visibility = View.INVISIBLE
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        val requestsRecyclerView = requests_list_recyclerview
        requestsRecyclerView.adapter = RequestsListAdapter(mutableListOf(), activity)

        val requestsLayoutManager = LinearLayoutManager(activity)
        requestsRecyclerView.layoutManager = requestsLayoutManager

        // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
        mDatabase?.child("students")?.child("one")?.child("requests")?.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val requests = mutableListOf<Request>()

                for (snapshot in dataSnapshot.children) {
                    val id = snapshot.key

                    val title = snapshot.child("title").value as String
                    val status = snapshot.child("status").value as String
                    val note = snapshot.child("note").value as String?

                    if (snapshot.hasChild("course_code") && snapshot.hasChild("course_name")) {
                        val courseCode = snapshot.child("course_code").value as String
                        val courseName = snapshot.child("course_name").value as String

                        requests.add(Request(id.toString(),title, status, note, courseCode, courseName))
                    } else {
                        requests.add(Request(id.toString(),title, status, note))
                    }
                }

                requestsRecyclerView.adapter = RequestsListAdapter(requests, activity)

                if (requests_empty_view != null && requestsRecyclerView.adapter.itemCount > 0) {
                    requests_empty_view.visibility = View.INVISIBLE
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

}