package tcc.ufc.br.maoamiga.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.*
import android.text.Editable
import android.text.TextWatcher
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Request
import tcc.ufc.br.maoamiga.model.RequestType
import tcc.ufc.br.maoamiga.model.Status
import kotlinx.android.synthetic.main.fragment_submit_request.*

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener

/**
* Created by Arthur Sousa on 05/04/18.
*/

class SubmitRequestFragment : Fragment(), AdapterView.OnItemSelectedListener {
    private var mDatabase: DatabaseReference? = null

    private var requestTypes: MutableList<RequestType> = ArrayList()
    private lateinit var selectedType: RequestType

    companion object {
        const val COURSE_CODE_LENGTH = 6
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDatabase = FirebaseDatabase.getInstance().reference

        (activity as BottomNavigationActivity).refreshBadge()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_submit_request, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        mDatabase?.child("request_types")?.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val enable = snapshot.child("enable").value as Boolean

                    if (!enable) {
                        continue
                    }

                    val name = snapshot.child("name").value as String

                    if (snapshot.hasChild("course_fields")) {
                        val courseFields = snapshot.child("course_fields").value as Boolean
                        requestTypes.add(RequestType(name, courseFields))
                    } else {
                        requestTypes.add(RequestType(name))
                    }
                }

                requestTypes = requestTypes.sortedBy { it.name }.toMutableList()

                val promptType = RequestType(getString(R.string.select))
                requestTypes.add(0, promptType)

                val otherType = RequestType(getString(R.string.other))
                requestTypes.add(otherType)

                val types = requestTypes.map { it.name }

                val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, types)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                requests_spinner.adapter = adapter
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })

        requests_spinner.onItemSelectedListener = this

        requestButton.setOnClickListener {
            sendData()
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        syllabusRequestSection.visibility = View.GONE
        otherRequestSection.visibility = View.GONE

        if (position == 0) {
            return
        }

        selectedType = requestTypes[position]

        if (selectedType.course_fields) {
            syllabusRequestSection.visibility = View.VISIBLE
            setSyllabusRequestListeners()

            disableSendButton()
            return
        }


        if (selectedType.name == getString(R.string.other)) {
            otherRequestSection.visibility = View.VISIBLE
            setOtherRequestListener()

            disableSendButton()
            return
        }

        enableSendButton()
    }

    private fun setSyllabusRequestListeners() {
        courseCodeEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                validateSyllabusFields()
            }
        })

        courseNameEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                validateSyllabusFields()
            }
        })
    }

    private fun validateSyllabusFields() {
        if (courseCodeEditText.text.length == COURSE_CODE_LENGTH && !courseNameEditText.text.isEmpty()) {
            enableSendButton()
        }
    }

    private fun setOtherRequestListener() {
        otherRequestEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                validateOtherField()
            }
        })
    }

    private fun validateOtherField() {
        if (!otherRequestEditText.text.isEmpty()) {
            enableSendButton()
        }
    }

    private fun enableSendButton() {
        requestButton.isEnabled = true
        requestButton.alpha = 1.0f
    }

    private fun disableSendButton() {
        requestButton.isEnabled = false
        requestButton.alpha = 0.5f
    }

    private fun sendData() {
        mDatabase?.let {
            // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
            val requestsRef = it.child("students").child("one").child("requests")

            val requestTitle = if (selectedType.name == getString(R.string.other)) {
                otherRequestEditText.text.toString()
            } else {
                selectedType.name
            }

            val newRequest = if (selectedType.course_fields) {
                Request("", requestTitle, Status.PENDING.value, noteEditText.text.toString(), courseCodeEditText.text.toString(),
                        courseNameEditText.text.toString())
            } else {
                Request("", requestTitle, Status.PENDING.value, noteEditText.text.toString())
            }

            requestsRef.push().setValue(newRequest, { _, databaseReference ->
                newRequest.id = databaseReference.key.toString()

                // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
                mDatabase?.child("students")?.child("one")?.child("requests")?.child(newRequest.id)?.setValue(newRequest)
            })

            refreshContent()
        }
    }

    private fun refreshContent() {
        val fragment = SubmitRequestFragment()

        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commit()

        Toast.makeText(activity, getString(R.string.request_sent), Toast.LENGTH_SHORT).show()
    }
}