package tcc.ufc.br.maoamiga.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import tcc.ufc.br.maoamiga.R
import kotlinx.android.synthetic.main.fragment_student.*

/**
* Created by Arthur Sousa on 05/04/18.
*/

class StudentFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as BottomNavigationActivity).refreshBadge()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_student, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // To do: Set student data from login authentication
        student_name.text = "Arthur Borges de Oliveira Sousa"
        student_enrollment_number.text = "382067"
        student_major.text = "Engenharia de Computação"

        logoffButton.setOnClickListener {
            // To do: Log off student
        }
    }

}