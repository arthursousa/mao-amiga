package tcc.ufc.br.maoamiga.ui.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.notification_item.view.*
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Notification

/**
* Created by Arthur Sousa on 15/04/18.
*/

class NotificationsListAdapter(private val notifications: MutableList<Notification>,
                               private val context: Context) : Adapter<NotificationsListAdapter.ViewHolder>() {

    private var mDatabase: DatabaseReference? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        mDatabase = FirebaseDatabase.getInstance().reference

        val view = LayoutInflater.from(context).inflate(R.layout.notification_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val notification = notifications[position]

        holder?.let {
            it.day.text = notification.day
            it.month.text = notification.month
            it.title.text = notification.title
            it.description.text = notification.description

            if (!notification.read) {
                it.notificationCardView.setBackgroundColor(ContextCompat.getColor(context, R.color.unread_notification))
            }
        }

        if (!notification.read) {
            notification.read = true

            // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
            mDatabase?.child("students")?.child("one")?.child("notifications")?.child(notification.id)?.setValue(notification)
        }
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val day: TextView = itemView.notification_item_day
        val month: TextView = itemView.notification_item_month
        val title: TextView = itemView.notification_item_title
        val description: TextView = itemView.notification_item_description

        val notificationCardView: CardView = itemView.notification_card_view
    }
}
