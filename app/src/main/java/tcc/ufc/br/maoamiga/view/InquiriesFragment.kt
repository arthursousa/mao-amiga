package tcc.ufc.br.maoamiga.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.widget.SearchView.OnQueryTextListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_inquiries.*
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Course
import tcc.ufc.br.maoamiga.ui.adapter.CoursesListAdapter

/**
* Created by Arthur Sousa on 05/04/18.
*/

class InquiriesFragment : Fragment() {
    private var mDatabase: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDatabase = FirebaseDatabase.getInstance().reference

        (activity as BottomNavigationActivity).refreshBadge()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_inquiries, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val searchView = course_searchview

        val coursesRecyclerView = courses_list_recyclerview
        coursesRecyclerView.adapter = CoursesListAdapter( mutableListOf(), activity)

        val coursesLayoutManager = LinearLayoutManager(activity)
        coursesRecyclerView.layoutManager = coursesLayoutManager

        searchView.setOnQueryTextListener(object : OnQueryTextListener {

            override fun onQueryTextChange(searchText: String): Boolean {
                if (searchText == "") {
                    courses_initial_view.visibility = View.VISIBLE
                    courses_header.visibility = View.INVISIBLE
                    courses_not_found_view.visibility = View.INVISIBLE

                    coursesRecyclerView.adapter = CoursesListAdapter( mutableListOf(), activity)
                } else {
                    courses_initial_view.visibility = View.INVISIBLE

                    // To Do: Use cloud functions para melhorar o desempenho dessa fitragem
                    mDatabase?.child("courses")?.addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val courses = mutableListOf<Course>()

                            for (snapshot in dataSnapshot.children) {
                                val nameContains = (snapshot.child("name").value as String).contains(searchText, true)
                                val codeContains = (snapshot.child("code").value as String).contains(searchText, true)

                                if (!nameContains && !codeContains) {
                                    continue
                                }

                                val code = snapshot.child("code").value as String
                                val name = snapshot.child("name").value as String
                                val equivalents = snapshot.child("equivalents").value as String
                                val requirements = snapshot.child("requirements").value as String
                                val privateSpots = snapshot.child("private_spots").value as String

                                courses.add(Course(code, name, equivalents, requirements, privateSpots))
                            }

                            if (courses.isEmpty()) {
                                courses_header.visibility = View.INVISIBLE
                                courses_not_found_view.visibility = View.VISIBLE

                                coursesRecyclerView.adapter = CoursesListAdapter( mutableListOf(), activity)
                                return

                            }

                            courses_header.visibility = View.VISIBLE
                            courses_not_found_view.visibility = View.INVISIBLE

                            coursesRecyclerView.adapter = CoursesListAdapter(courses, activity)
                        }

                        override fun onCancelled(databaseError: DatabaseError) {}
                    })
                }

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })
    }

}