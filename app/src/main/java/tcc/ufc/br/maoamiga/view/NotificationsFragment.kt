package tcc.ufc.br.maoamiga.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.os.Handler
import tcc.ufc.br.maoamiga.R
import tcc.ufc.br.maoamiga.model.Notification
import tcc.ufc.br.maoamiga.ui.adapter.NotificationsListAdapter
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_notifications.*

import com.google.firebase.database.*


/**
* Created by Arthur Sousa on 05/04/18.
*/

class NotificationsFragment : Fragment() {
    private var mDatabase: DatabaseReference? = null

    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDatabase = FirebaseDatabase.getInstance().reference

        (activity as BottomNavigationActivity).removeBadge()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshNotifications()

        // Initialize the handler instance
        mHandler = Handler()

        swipe_refresh_layout.setOnRefreshListener {
            mRunnable = Runnable {
                refreshNotifications(true)
            }

            // Execute the task after specified time
            mHandler.postDelayed(
                    mRunnable, (2000).toLong()
            )
        }
    }

    private fun refreshNotifications(swypedUp: Boolean = false) {
        val notificationsRecyclerView = notifications_list_recyclerview
        notificationsRecyclerView.adapter = NotificationsListAdapter(mutableListOf(), activity)

        val notificationsLayoutManager = LinearLayoutManager(activity)
        notificationsRecyclerView.layoutManager = notificationsLayoutManager

        // To Do: make it relative to studentId after authentication -> "it.child("students").child(studentId)"
        mDatabase?.child("students")?.child("one")?.child("notifications")?.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val notifications = mutableListOf<Notification>()

                for (snapshot in dataSnapshot.children) {
                    val id = snapshot.key

                    val day = snapshot.child("day").value as String
                    val month = snapshot.child("month").value as String
                    val title = snapshot.child("title").value as String
                    val description = snapshot.child("description").value as String
                    val read = snapshot.child("read").value as Boolean

                    notifications.add(Notification(id.toString(), day, month, title, description, read))
                }

                notificationsRecyclerView.adapter = NotificationsListAdapter(notifications, activity)

                if (notifications_empty_view != null && notificationsRecyclerView.adapter.itemCount > 0) {
                    notifications_empty_view.visibility = View.INVISIBLE
                }

                if (swypedUp) {
                    // Hide swipe to refresh icon animation
                    swipe_refresh_layout.isRefreshing = false
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

}